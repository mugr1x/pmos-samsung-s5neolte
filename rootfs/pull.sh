for d in $(adb shell find / -maxdepth 1 -type d);do
  adb pull -Z "$d"k
  if [[ $d != "/" && $d != "/proc" && $d != "/sys" && $d != "/system" ]];then
    echo $d
    adb pull $d
  fi
done
