## Device port for Galaxy S5 Neo Exynos smartphone

I unfortunately do no longer own the phone, so I instead decided to publish related files and
try to point out some directions to go if anyone wants to make a PostmarketOS port.
First you would want to have a look at the following websites:

[Sammobile](https://sammobile.com)
[XDA](https://forum.xda-developers.com)
[OpenGapps](https://opengapps.org)
[wiki](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_S5_Neo_(samsung-s5neolte))

It relativley easy to get LineageOS running on this device and (18.1 worked for me).
Just get Heimdall (unix derivates) or Odin (Windows) and flash the system partition of
your device after putting it into download mode. For this press home-power-volume_up.
If you just want to have TWRP apply the some process to recovery partition and the TWRP image.
There was also someone already starting to port this device to PostmarketOS. Have fun !
