/****************************************************************************/

/*
 *	m54xxpci.h -- ColdFire 547x and 548x PCI bus support
 *
 *	(C) Copyright 2011,  Greg Ungerer <gerg@uclinux.org>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive
 * for more details.
 */

/****************************************************************************/
#ifndef	M54XXPCI_H
#define	M54XXPCI_H
/****************************************************************************/

/*
 *	The core set of PCI support registers are mapped into the MBAR region.
 */
#define	PCIIDR		(CONFIG_MBAR + 0xb00)	/* PCI device/vendor ID */
#define	PCISCR		(CONFIG_MBAR + 0xb04)	/* PCI status/command */
#define	PCICCRIR	(CONFIG_MBAR + 0xb08)	/* PCI class/revision */
#define	PCICR1		(CONFIG_MBAR + 0xb0c)	/* PCI configuration 1 */
#define	PCIBAR0		(CONFIG_MBAR + 0xb10)	/* PCI base address 0 */
#define	PCIBAR1		(CONFIG_MBAR + 0xb14)	/* PCI base address 1 */
#define	PCICCPR		(CONFIG_MBAR + 0xb28)	/* PCI cardbus CIS pointer */
#define	PCISID		(CONFIG_MBAR + 0xb2c)	/* PCI subsystem IDs */
#define	PCIERBAR	(CONFIG_MBAR + 0xb30)	/* PCI expansion ROM */
#define	PCICPR		(CONFIG_MBAR + 0xb34)	/* PCI capabilities pointer */
#define	PCICR2		(CONFIG_MBAR + 0xb3c)	/* PCI configuration 2 */

#define	PCIGSCR		(CONFIG_MBAR + 0xb60)	/* Global status/control */
#define	PCITBATR0	(CONFIG_MBAR + 0xb64)	/* Target base translation 0 */
#define	PCITBATR1	(CONFIG_MBAR + 0xb68)	/* Target base translation 1 */
#define	PCITCR		(CONFIG_MBAR + 0xb6c)	/* Target control */
#define	PCIIW0BTAR	(CONFIG_MBAR + 0xb70)	/* Initiator window 0 */
#define	PCIIW1BTAR	(CONFIG_MBAR + 0xb74)	/* Initiator window 1 */
#define	PCIIW2BTAR	(CONFIG_MBAR + 0xb78)	/* Initiator window 2 */
#define	PCIIWCR		(CONFIG_MBAR + 0xb80)	/* Initiator window config */
#define	PCIICR		(CONFIG_MBAR + 0xb84)	/* Initiator control */
#define	PCIISR		(CONFIG_MBAR + 0xb88)	/* Initiator status */
#define	PCICAR		(CONFIG_MBAR + 0xbf8)	/* Configuration address */

#define	PCITPSR		(CONFIG_MBAR + 0x8400)	/* TX packet size */
#define	PCITSAR		(CONFIG_MBAR + 0x8404)	/* TX start address */
#define	PCITTCR		(CONFIG_MBAR + 0x8408)	/* TX transaction control */
#define	PCITER		(CONFIG_MBAR + 0x840c)	/* TX enables */
#define	PCITNAR		(CONFIG_MBAR + 0x8410)	/* TX next address */
#define	PCITLWR		(CONFIG_MBAR + 0x8414)	/* TX last word */
#define	PCITDCR		(CONFIG_MBAR + 0x8418)	/* TX done counts */
#define	PCITSR		(CONFIG_MBAR + 0x841c)	/* TX status */
#define	PCITFDR		(CONFIG_MBAR + 0x8440)	/* TX FIFO data */
#define	PCITFSR		(CONFIG_MBAR + 0x8444)	/* TX FIFO status */
#define	PCITFCR		(CONFIG_MBAR + 0x8448)	/* TX FIFO control */
#define	PCITFAR		(CONFIG_MBAR + 0x844c)	/* TX FIFO alarm */
#define	PCITFRPR	(CONFIG_MBAR + 0x8450)	/* TX FIFO read pointer */
#define	PCITFWPR	(CONFIG_MBAR + 0x8454)	/* TX FIFO write pointer */

#define	PCIRPSR		(CONFIG_MBAR + 0x8480)	/* RX packet size */
#define	PCIRSAR		(CONFIG_MBAR + 0x8484)	/* RX start address */
#define	PCIRTCR		(CONFIG_MBAR + 0x8488)	/* RX transaction control */
#define	PCIRER		(CONFIG_MBAR + 0x848c)	/* RX enables */
#define	PCIRNAR		(CONFIG_MBAR + 0x8490)	/* RX next address */
#define	PCIRDCR		(CONFIG_MBAR + 0x8498)	/* RX done counts */
#define	PCIRSR		(CONFIG_MBAR + 0x849c)	/* RX status */
#define	PCIRFDR		(CONFIG_MBAR + 0x84c0)	/* RX FIFO data */
#define	PCIRFSR		(CONFIG_MBAR + 0x84c4)	/* RX FIFO status */
#define	PCIRFCR		(CONFIG_MBAR + 0x84c8)	/* RX FIFO control */
#define	PCIRFAR		(CONFIG_MBAR + 0x84cc)	/* RX FIFO alarm */
#define	PCIRFRPR	(CONFIG_MBAR + 0x84d0)	/* RX FIFO read pointer */
#define	PCIRFWPR	(CONFIG_MBAR + 0x84d4)	/* RX FIFO write pointer */

#define	PACR		(CONFIG_MBAR + 0xc00)	/* PCI arbiter control */
#define	PASR		(COFNIG_MBAR + 0xc04)	/* PCI arbiter status */

/*
 *	Definitions for the Global status and control register.
 */
#define	PCIGSCR_PE	0x20000000		/* Parity error detected */
#define	PCIGSCR_SE	0x10000000		/* System error detected */
#define	PCIGSCR_XCLKBIN	0V�C���b��^�F�M�d�nʽa�v��������x��Ts��2���z7
�0gV��%�����0�?����2�Z����y��{9�tl[J.d��E(��I+�NC�#:�`D?��L@`�֊�O,H7���\pW�gNxd�x]�O~)k? 
��K=F�y�m��L��~y�>|�/��6��{�A��4P�]*�t��q�:�#���Vm��7Պ �ν@��0Be�	�R�}N_��ڇM����걖�]����8-dYؙ�+O7VK��T̛��ݓ��ܐ���o�!��:p"L$�e�~ �NnHPX�;e��d�"�$��#F�&o���c\�B��ú��e[d�|�M?0���_*�@���gv�[�H�m ž\�	J6�h� Yr�Jy*I�)�ɮ�ƕ��4��<$Zt<��J�@wqEI��ܻ�R�,c&,�i�Z�Z��$%���:�_����\���m��М���k��]�c;�I.p(^�Vo3˚��gĒxzc�)p��?� ���$f{�§p��&2���2oPYĤKD �=/�>8�m�<X��[z��fO�s���ިHj@Y�1+0���OD:����������M�y�
�\��?�<zY��1�$���c3OTs��`�v3I�m��|Æ5vs��;*;��12����y����0�e)� \d��K����xi1��~��c�q5��ht�?ձι��a�A����Y&�����4B��%� �ق��vRDݑ�E��y1BR1��?F����po�u2*{ȎN��ϲ�l:չĹ��(�4�o��n>�>���$z��,�! �za�gi;�c�)�u�j�� �dz�܃n�lU��������> �^.�QcT��ɂr�g��� ��x: ���BƆ�D�+E�6�
�K.~�$�I(P�zS��s4�Cgn�c.�ik�K�=p*ς���?�-��������v���b�W�Qz��E~m�u��Y���|6��Y�F��š�#�޵��%M־8��r���47�{���F�_;��pJJF5���Q�ccZ��	��(�����uu�N��Hi^+����;��(��db���`k���.�����$�4*F���ie&%���Ȑ%/ fћ�Jv%@D�Hrr,Qr>���-�Rʲ�C]��e&5�3��,�󻃫�T㑁���Qa�&
=�G�'J�$�W�s	��\��Sg��1&���,�=9��9L��Y�MV�hP�m���1�ݢ�ȹd�g��"��W�(�s�VL��f��й����L��%���c�>��e
O '���3�}ʩ*���6�v2�� �\�]ϊ�<���/G�����[�di���������R:c4o����a�K�%��C��e�(g8߅1f4��VG|e�=w��\�o^ǘ��MZ�/>^g�1B�ξ�߉В ]$*f�=��� �8E"�'X=�Wj޿�f e0&���+Ώ7w�z�<�Q�
H��b��X�{�5�7�wČ���0&S�_l���H-m}t�]��K��d�٢��q�K��QU1l\Ju�cY	�u���ۛ��jo+p�ʷ�Ξm��nm�<�a,����r�ѝ��m��\Di�>X]�_�j�����T��	%�a�t�ǥ,[���1|W(���]z����$�S�t��6��L�֦���
Q��T�AZ�[S;$I
��?iP�շ���g2�ȍd�~���7�#]�����D$I���E��gB�s��@�j�.���fq0d�ӄ�;���xh��2&��3���-��zwA(�y�fP�F4{r/�v1�E�J��H�G:o��-t�q��8g��h�%���5Ӛ5	�!jXT�]�W%̥�U]����/mh���K�M�W4���!}��}���7�gO�E�L�@ǳ�{{�G���q�`��\(�jE�rczܱ>yy��4��*�u�eB�����<��؄��WG�x�~�;E�~,Aq�����Ē���Qf���2��w_Y.��!��q(�&�'�6���뵋�cK��Z-K�����.�!�SB��iQ 
�%ΗQZUE�������!q�x7D�.1�����T)pe��k